package com.businesssarathi.androidmobiledevelopment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class TabUsingFragmentActivity extends AppCompatActivity {

    TextView tab1, tab2, tab3;

    TopFragment topFragment;
    BottomFragment bottomFragment;
    Tab3Fragment tab3Fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_using_fragment);
        tab1 = findViewById(R.id.tab1);
        tab2 = findViewById(R.id.tab2);
        tab3 = findViewById(R.id.tab3);

        topFragment = new TopFragment();
        bottomFragment = new BottomFragment();
        tab3Fragment = new Tab3Fragment();
        tab2.setTextColor(Color.BLACK);
        tab3.setTextColor(Color.BLACK);
        tab1.setTextColor(Color.BLUE);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, topFragment).commit();

    }

    public void tabclickListener(View view){
        tab1.setTextColor(Color.BLACK);
        tab2.setTextColor(Color.BLACK);
        tab3.setTextColor(Color.BLACK);
        if(view.getId()==R.id.tab1){
            tab1.setTextColor(Color.BLUE);
            getSupportFragmentManager().beginTransaction().replace(R.id.container, topFragment).commit();
        }else if(view.getId()==R.id.tab2){
            tab2.setTextColor(Color.BLUE);
            getSupportFragmentManager().beginTransaction().replace(R.id.container, bottomFragment).commit();
        }else if(view.getId()==R.id.tab3){
            tab3.setTextColor(Color.BLUE);
            getSupportFragmentManager().beginTransaction().replace(R.id.container, tab3Fragment).commit();
        }
    }


}