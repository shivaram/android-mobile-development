package com.businesssarathi.androidmobiledevelopment;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.businesssarathi.androidmobiledevelopment.helper.Userinfo;

import java.util.ArrayList;

public class UserlistAdapter extends ArrayAdapter<Userinfo> {
    Context context;

    public UserlistAdapter(@NonNull Context context, ArrayList<Userinfo> list) {
        super(context, 0,list);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_item_layout, null);

        TextView username = view.findViewById(R.id.username);
        TextView email = view.findViewById(R.id.email);
        TextView adress = view.findViewById(R.id.address);

        Userinfo info = getItem(position);

        username.setText(info.username);
        email.setText(info.email);
        adress.setText(info.address);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,DetailActivity.class);
                intent.putExtra("id",info.id);
                context.startActivity(intent);
            }
        });
        return view;
    }
}
