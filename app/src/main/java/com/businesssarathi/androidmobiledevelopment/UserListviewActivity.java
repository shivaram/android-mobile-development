package com.businesssarathi.androidmobiledevelopment;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class UserListviewActivity extends AppCompatActivity {

    ListView listView;
    GridView gridView;
    UserlistAdapter adapter;
    UserGridAdapter gridAdapter;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_listview);
        listView = findViewById(R.id.listview);
        gridView = findViewById(R.id.gridview);

        databaseHelper = new DatabaseHelper(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter = new UserlistAdapter(this, databaseHelper.getUserList());
        gridAdapter = new UserGridAdapter(this, databaseHelper.getUserList());
        listView.setAdapter(adapter);
        listView.setVisibility(View.GONE);
        gridView.setAdapter(gridAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_list_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.grid){
            listView.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
        }else{
            listView.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.GONE);
        }
        return super.onOptionsItemSelected(item);
    }
}