package com.businesssarathi.androidmobiledevelopment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.businesssarathi.androidmobiledevelopment.helper.Userinfo;

import java.util.ArrayList;

public class UserListActivity extends AppCompatActivity {

    LinearLayout userListContainer;
    DatabaseHelper databaseHelper;

    ArrayList<Userinfo>userList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseHelper = new DatabaseHelper(this);
        userListContainer = findViewById(R.id.userListcontainer);

        userList = databaseHelper.getUserList();

        displayUsers();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_list_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }else if(item.getItemId()==R.id.list){

        }else if(item.getItemId()==R.id.grid){

        }
        return super.onOptionsItemSelected(item);
    }

    public void displayUsers(){
        for (int i = 0 ; i<userList.size();i++){
            Userinfo info = userList.get(i);
        }

        for (Userinfo info:userList){
            View view = LayoutInflater.from(this).inflate(R.layout.list_item_layout,null);

            TextView username = view.findViewById(R.id.username);
            TextView email = view.findViewById(R.id.email);
            TextView adress = view.findViewById(R.id.address);
            username.setText(info.username);
            email.setText(info.email);
            adress.setText(info.address);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(UserListActivity.this,DetailActivity.class);
                    intent.putExtra("id",info.id);
                    startActivity(intent);

                }
            });


            userListContainer.addView(view);

        }


    }


}