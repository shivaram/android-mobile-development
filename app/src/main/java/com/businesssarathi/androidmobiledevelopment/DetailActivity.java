package com.businesssarathi.androidmobiledevelopment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.businesssarathi.androidmobiledevelopment.helper.Userinfo;

public class DetailActivity extends AppCompatActivity {

    String id;
    TextView username, email, address, phone, gender;
    Button update, delete;
    DatabaseHelper databaseHelper;
    ImageView imageView,image1;
    String url = "https://camo.githubusercontent.com/09b77bd11b3f4ae551b08bf0dec47675b958c728916c0c500ee2d1778a82a403/68747470733a2f2f7261772e6769746875622e636f6d2f68646f64656e686f662f436972636c65496d616765566965772f6d61737465722f73637265656e73686f742e706e67";

    Userinfo info;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        id = getIntent().getStringExtra("id");

        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        address = findViewById(R.id.address);
        phone = findViewById(R.id.phone);
        gender = findViewById(R.id.gender);
        update = findViewById(R.id.update);
        delete = findViewById(R.id.delete);
        imageView = findViewById(R.id.imageView);
        image1 = findViewById(R.id.image1);
        Glide.with(this)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_android_black_24dp)
                .into(image1);

        databaseHelper = new DatabaseHelper(this);


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, RegisterActivity.class);
                intent.putExtra("id", Integer.parseInt(id));
                startActivity(intent);
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog();
            }
        });


    }

    public void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete user");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                databaseHelper.deleteUser(id);
                finish();
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        info = databaseHelper.getUserinfo(id);

        username.setText(info.username);
        address.setText(info.address);
        phone.setText(info.phone);
        gender.setText(info.gender);
        email.setText(info.email);
        if (info.image != null)
            imageView.setImageBitmap(DatabaseHelper.getBitmap(info.image));
    }
}