package com.businesssarathi.androidmobiledevelopment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class HomeActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.i("Lifecycle", "onCreate");

        sharedPreferences = getSharedPreferences("Userinfo", Context.MODE_PRIVATE);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences.edit().putBoolean("rememberme", false).commit();
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu1:
                startActivity(new Intent(HomeActivity.this, RegisterActivity.class));
                break;
            case R.id.menu2:
                showAlertDialog();
                break;
            case R.id.menu3:
                startActivity(new Intent(HomeActivity.this, FragmentExampleActivity.class));
                Toast.makeText(this, "Menu3", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu4:
                startActivity(new Intent(HomeActivity.this, UserListActivity.class));
                showCustomToast("This is custom toast");
                break;
            case R.id.menu5:

                break;
            case R.id.submenu:
                startActivity(new Intent(HomeActivity.this, UserListviewActivity.class));
                showCustomToast("submenu");
                break;
            case R.id.submenu1:
                startActivity(new Intent(HomeActivity.this,TabUsingFragmentActivity.class));
                showCustomToast("submenu1");
                break;
            case R.id.submenu2:
                showCustomToast("submenu2");
                startActivity(new Intent(HomeActivity.this,WebviewActivity.class));
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    public void showAlertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Exit application!");
        dialog.setMessage("Are you sure?");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.setNegativeButton("Cancel", null);
        dialog.setCancelable(false);
        dialog.show();
    }


    public void showCustomToast(String srt) {
        Toast toast = new Toast(this);
        View view = LayoutInflater.from(this).inflate(R.layout.custom_toast_design, null);
        TextView message = view.findViewById(R.id.message);
        message.setText(srt);
        toast.setView(view);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 100);
        toast.show();


    }


}