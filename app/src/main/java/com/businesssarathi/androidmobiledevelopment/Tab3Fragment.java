package com.businesssarathi.androidmobiledevelopment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class Tab3Fragment extends Fragment {

    ListView listView;
    GridView gridView;

    UserlistAdapter adapter;
    UserGridAdapter gridAdapter;
    DatabaseHelper databaseHelper;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_user_listview, null);
        listView = view.findViewById(R.id.listview);
        gridView = view.findViewById(R.id.gridview);
        databaseHelper = new DatabaseHelper(getActivity());
        adapter = new UserlistAdapter(getActivity(), databaseHelper.getUserList());
        gridAdapter = new UserGridAdapter(getActivity(), databaseHelper.getUserList());
        listView.setAdapter(adapter);
        listView.setVisibility(View.GONE);
        gridView.setAdapter(gridAdapter);

        return view;

    }
}
