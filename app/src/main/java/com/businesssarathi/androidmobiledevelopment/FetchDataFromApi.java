package com.businesssarathi.androidmobiledevelopment;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.businesssarathi.androidmobiledevelopment.helper.Userinfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FetchDataFromApi extends AppCompatActivity {

    ListView listView;
    TextView response;
    String url = "http://10.0.2.2/amd/select.php";// server and emulator same pc
//    String url = "http://192.168.0.135/amd/select.php";// server and mobile differnet pc
//    String url = "http://prakas.com.np/amd/select.php";// live server

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_data_from_api);
        response = findViewById(R.id.response);
        listView = findViewById(R.id.listview);
        fetchData();
    }

    public void fetchData() {


// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);


// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respon) {
                        // Display the first 500 characters of the response string.
//                        response.setText("Response is: " + respon);
                        parseResponse(respon);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                response.setText("That didn't work!");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void parseResponse(String response) {
        ArrayList<Userinfo> list = new ArrayList<Userinfo>();
        try {
            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                Userinfo info = new Userinfo();
                JSONObject object = array.getJSONObject(i);
                info.id = object.getString("id");
                info.username = object.getString("username");
                info.password = object.getString("password");
                info.email = object.getString("email");
                info.address = object.getString("address");
                info.phone = object.getString("phone");
                info.gender = object.getString("gender");
                list.add(info);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        listView.setAdapter(new UserlistAdapter(this, list));
    }
}