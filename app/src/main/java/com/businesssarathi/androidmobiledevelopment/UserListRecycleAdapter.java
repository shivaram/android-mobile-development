package com.businesssarathi.androidmobiledevelopment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.businesssarathi.androidmobiledevelopment.helper.Userinfo;

import java.util.ArrayList;

public class UserListRecycleAdapter extends RecyclerView.Adapter<UserListRecycleAdapter.RecycleViewHolder> {

    Context context;
    ArrayList<Userinfo> list;

    public UserListRecycleAdapter(Context context, ArrayList<Userinfo> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_layout, null);
        return new RecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder holder, int position) {
        Userinfo info = list.get(position);
        holder.username.setText(info.username);
        holder.email.setText(info.email);
        holder.address.setText(info.address);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class RecycleViewHolder extends RecyclerView.ViewHolder {
        TextView username, email, address;

        public RecycleViewHolder(@NonNull View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.username);
            email = itemView.findViewById(R.id.email);
            address = itemView.findViewById(R.id.address);
        }
    }
}
