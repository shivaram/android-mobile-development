package com.businesssarathi.androidmobiledevelopment;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.businesssarathi.androidmobiledevelopment.helper.Userinfo;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText username, password, email, address, phone;
    RadioGroup gender;
    Button register;

    ImageView imageView;
    LinearLayout form;

    DatabaseHelper databaseHelper;

    SharedPreferences sharedPreferences;

    int[] colors = new int[]{Color.RED, Color.GRAY, Color.GREEN, Color.BLUE, Color.YELLOW, Color.MAGENTA};
    int i = 0;
    int id;

    String url = "http://10.0.2.2/amd/insert.php";
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_form);
        id = getIntent().getIntExtra("id", 0);

        sharedPreferences = getSharedPreferences("Userinfo", Context.MODE_PRIVATE);
        databaseHelper = new DatabaseHelper(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        form = findViewById(R.id.form);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        email = findViewById(R.id.email);
        address = findViewById(R.id.address);
        phone = findViewById(R.id.phone);
        gender = findViewById(R.id.gender);
        register = findViewById(R.id.register);
        register.setBackgroundColor(Color.RED);
        imageView = findViewById(R.id.image);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 101);

            }
        });

        if (id != 0) {
            Userinfo info = databaseHelper.getUserinfo(String.valueOf(id));
            username.setText(info.username);
            address.setText(info.address);
            phone.setText(info.phone);
            password.setText(info.password);
            if (info.gender.equals("Male")) {
                ((RadioButton) findViewById(R.id.male)).setChecked(true);
            } else {
                ((RadioButton) findViewById(R.id.female)).setChecked(true);
            }
            email.setText(info.email);
            register.setText("Update");
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (emptyFieldValidation(username) && emptyFieldValidation(password) && isvalidEmail(email)) {

                    String usernameValue = username.getText().toString();
                    String passwordValue = password.getText().toString();
                    String emailValue = email.getText().toString();
                    String addressValue = address.getText().toString();
                    String phoneValue = phone.getText().toString();

                    RadioButton checkedBtn = findViewById(gender.getCheckedRadioButtonId());
                    String genderValue = checkedBtn.getText().toString();

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("username", usernameValue);
                    editor.putString("password", passwordValue);
                    editor.putString("email", emailValue);
                    editor.putString("address", addressValue);
                    editor.putString("phone", phoneValue);
                    editor.putString("gender", genderValue);
                    editor.commit();

                    ContentValues contentValues = new ContentValues();
                    contentValues.put("username", usernameValue);
                    contentValues.put("password", passwordValue);
                    contentValues.put("email", emailValue);
                    contentValues.put("address", addressValue);
                    contentValues.put("phone", phoneValue);
                    contentValues.put("gender", genderValue);
                    if (bitmap != null)
                        contentValues.put("image", DatabaseHelper.getBlob(bitmap));
                    fetchData();
                    if (id == 0) {

                        databaseHelper.insertUser(contentValues);

                        Toast.makeText(RegisterActivity.this, "User Registered", Toast.LENGTH_SHORT).show();
                    } else {
                        databaseHelper.updateUser(id + "", contentValues);
                        Toast.makeText(RegisterActivity.this, "User updated", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            }
        });

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                form.setBackgroundColor(colors[i]);
                if (i < 5)
                    i++;
                else
                    i = 0;
            }
        });
    }

    public boolean emptyFieldValidation(EditText view) {
        if (view.getText().toString().length() > 6) {
            return true;
        } else {
            view.setError("Minimum length must be 6");
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isvalidEmail(EditText view) {
        String emailValue = view.getText().toString();
        if (Patterns.EMAIL_ADDRESS.matcher(emailValue).matches()) {
            return true;
        } else {
            view.setError("INvalid email address");
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {
            bitmap = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(bitmap);

        }


    }

    public void fetchData() {


        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respon) {
                        // Display the first 500 characters of the response string.
//                        response.setText("Response is: " + respon);
                        Log.i("response0", "reson" + respon);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }

        ) {
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("username", username.getText().toString());
                params.put("password", password.getText().toString());
                params.put("email", email.getText().toString());
                params.put("address", address.getText().toString());
                params.put("phone", phone.getText().toString());
                return params;
            }
        };

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}