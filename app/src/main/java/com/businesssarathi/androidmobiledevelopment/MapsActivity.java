package com.businesssarathi.androidmobiledevelopment;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    PolylineOptions polylineOptions;
    SharedPreferences sharedPreferences;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        sharedPreferences = getSharedPreferences("Map", 0);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
//        sharedPreferences.edit().clear().apply();
        Log.i("Map", googleMap.toString());
        mMap = googleMap;
        String latitude = sharedPreferences.getString("lat", "0");
        String longitudeValu = sharedPreferences.getString("lang", "0");

        LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitudeValu));
        mMap.addMarker(new MarkerOptions().position(latLng).title("Last saved location"));


        findViewById(R.id.type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMap.getMapType() == GoogleMap.MAP_TYPE_HYBRID) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                } else {
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                }
            }
        });

        // Add a marker in Sydney and move the camera

        LatLng nepal = new LatLng(27.6875, 85.355);
        mMap.addMarker(new MarkerOptions().position(nepal).title("Nepal"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(nepal, 13));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.addCircle(new CircleOptions().center(nepal).radius(200).strokeColor(Color.RED));
        polylineOptions = new PolylineOptions();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(@NonNull LatLng latLng) {
                mMap.clear();
                polylineOptions.add(latLng);
                mMap.addMarker(new MarkerOptions().position(latLng).title("New position:" + latLng.latitude + ":" + latLng.longitude));
                mMap.addCircle(new CircleOptions().center(latLng).radius(200).strokeColor(Color.RED));
                mMap.addPolyline(polylineOptions);

                double lat = latLng.latitude;
                double longitude = latLng.longitude;
                sharedPreferences.edit().putString("lat", lat + "").apply();
                sharedPreferences.edit().putString("lang", longitude + "").apply();


            }
        });
        showMylocation();


    }

    public void showMylocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 11);
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        showMylocation();

    }
}