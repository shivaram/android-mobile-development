package com.businesssarathi.androidmobiledevelopment.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.businesssarathi.androidmobiledevelopment.DatabaseHelper;
import com.businesssarathi.androidmobiledevelopment.R;
import com.businesssarathi.androidmobiledevelopment.UserListRecycleAdapter;

public class HomeFragment extends Fragment {

    RecyclerView recyclerView;
    DatabaseHelper databaseHelper;
    UserListRecycleAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(getActivity());
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = root.findViewById(R.id.recycleView);
        adapter = new UserListRecycleAdapter(getActivity(), databaseHelper.getUserList());
        Log.i("userlist","list:"+databaseHelper.getUserList().size());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        return root;
    }
}